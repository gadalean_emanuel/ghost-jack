package com.example.secondapp;

import android.graphics.Canvas;

public interface GameObject {

    void draw(Canvas canvas);

    void update();

}
